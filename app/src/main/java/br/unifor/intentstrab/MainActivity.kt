package br.unifor.intentstrab

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.provider.MediaStore
import android.graphics.Bitmap
import android.provider.ContactsContract


class MainActivity : AppCompatActivity(), View.OnClickListener {

    //Intents Constants
    val CAMERA_ACTIVITY_ID = 1
    val CONTACT_ACTIVITY_ID = 2

    //Elements Ref
    lateinit var mButtonCamera: Button
    lateinit var mButtonContacts: Button
    lateinit var mButtonCall: Button
    lateinit var mImageView: ImageView
    lateinit var mTextName: TextView
    lateinit var mTextPhone: TextView

    private var uriContact: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Init Refs
        mButtonCamera = findViewById(R.id.main_button_camera) as Button
        mButtonCamera.setOnClickListener(this)

        mButtonContacts = findViewById(R.id.main_button_contacts) as Button
        mButtonContacts.setOnClickListener(this)

        mButtonCall = findViewById(R.id.main_button_call) as Button
        mButtonCall.setOnClickListener(this)

        mImageView = findViewById(R.id.main_imageView_camera) as ImageView

        mTextName = findViewById(R.id.main_textView_contacts_name) as TextView
        mTextPhone = findViewById(R.id.main_textView_contacts_phone) as TextView

    }

    //On Clicks

    override fun onClick(view: View?) {
        when(view?.id){
            R.id.main_button_camera -> onClickCamera()
            R.id.main_button_contacts -> onClickContacts()
            R.id.main_button_call -> onClickCall()
        }

    }

    //Ação de quando clica no botão Camera
    private fun onClickCamera(){
        val it = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (it.resolveActivity(packageManager) != null) {
            startActivityForResult(it, CAMERA_ACTIVITY_ID);
        }

    }

    //Ação de quando clica no botão Contacts
    private fun onClickContacts(){
        val it = Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI)
        if (it.resolveActivity(packageManager) != null) {
            startActivityForResult(it, CONTACT_ACTIVITY_ID)
        }
    }

    //Ação de quando clica no botão Call Contact
    private fun onClickCall(){
        val it = Intent(Intent.ACTION_DIAL)
        it.data = Uri.parse("tel:${mTextPhone.text}")
        if (it.resolveActivity(packageManager) != null) {
            startActivity(it)
        }
    }

    //Retorno de Activities

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when(requestCode){
            CAMERA_ACTIVITY_ID -> updateViewImage(resultCode, data)
            CONTACT_ACTIVITY_ID -> updateContactsTexts(resultCode, data)
        }
    }

    //Atualiza a imagem com a foto retornada pela camera
    private fun updateViewImage(resultCode: Int, data: Intent?){
        when(resultCode){
            Activity.RESULT_OK -> {
                val cameraImageBitmap = data?.extras?.get("data") as Bitmap
                mImageView.setImageBitmap(cameraImageBitmap)
            }
        }
    }

    //Atualiza os textos dos contatos retornados.
    private fun updateContactsTexts(resultCode: Int, dataIt: Intent?){
        when(resultCode){
            Activity.RESULT_OK -> {

                uriContact = dataIt?.data

                mTextName.text = "Name: ${retrieveContactName()}"
                mTextPhone.text = "Phone: ${retrieveContactNumber()}"

            }
        }
    }

    private fun retrieveContactName():String {

        var contactName:String = ""
        val cursor = contentResolver.query(uriContact, null, null, null, null)

        if (cursor!!.moveToFirst()) {
            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
        }

        cursor.close()

        return contactName
    }

    private fun retrieveContactNumber():String {

        var contactNumber: String = ""
        var contactID:String = ""

        // getting contacts ID
        val cursorID = contentResolver.query(uriContact,
                arrayOf(ContactsContract.Contacts._ID), null, null, null)

        if (cursorID!!.moveToFirst()) {
            contactID = cursorID.getString(cursorID.getColumnIndex(ContactsContract.Contacts._ID))
        }

        cursorID.close()

        // Using the contact ID now we will get contact phone number
        val cursorPhone = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                arrayOf(ContactsContract.CommonDataKinds.Phone.NUMBER),

                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ? AND " +
                        ContactsContract.CommonDataKinds.Phone.TYPE + " = " +
                        ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE,

                arrayOf(contactID),
                null)

        if (cursorPhone!!.moveToFirst()) {
            contactNumber = cursorPhone.getString(cursorPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
        }

        cursorPhone.close()

        return contactNumber
    }

}
